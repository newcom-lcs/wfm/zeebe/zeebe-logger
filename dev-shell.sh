# if [ -z "$API_DB_HOST" ]; then
#     echo no se estableció una IP para conexión a BD
#     echo para establecer la IP utilizar:
#     echo "          API_DB_HOST=ip_de_la_bd $0"
#     exit
# fi

path_proyecto=`git remote get-url origin | sed 's/:/\//g' | sed 's/^git@//' | sed 's/.git$//'`
export GIT_PROJECT_PATH=$path_proyecto


docker-compose up -d
docker-compose exec api sh
docker-compose down

