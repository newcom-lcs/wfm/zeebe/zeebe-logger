echo inicializando submodulos
git submodule init
git submodule update --remote

path_proyecto=`git remote get-url origin | sed 's/:/\//g' | sed 's/^git@//' | sed 's/.git$//'`
export GIT_PROJECT_PATH=$path_proyecto
echo "$path_proyecto"

volume=$(grep -P "API_BIN_VOLUME=.+" .env | grep -Po "[^=]+$")
docker volume create --name=$volume

docker-compose up -d
# docker-compose exec api sh -c "go get && go build"
docker-compose exec api sh -c "go get && go build -i && go install"
docker-compose down -v