## Dockderfile para aplicaciones golang
## Version 2.1
FROM golang:alpine as api
ADD . /go/src/api
WORKDIR /go/src/api
RUN go mod vendor 
RUN go build
RUN go install


FROM alpine
WORKDIR /app
COPY --from=api /go/bin/* /app/api
ADD migrate /app/migrate
ENTRYPOINT [ "/app/api"]


