## Inicializar Proyecto
`./init-repo.sh`

## Iniciar entorno de desarrollo
`./dev-shell.sh`

 Donde `xx.xx.xx.xx` es la ip de la BD de datos

### Una vez dentro del entorno:
`go get` para instalar dependencias
`./build.sh` para compilar



