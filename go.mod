module gitlab.com/newcom-lcs/clientes/prisma/rm/api.recursos-atm

replace gitlab.com/newcom-lcs/core/nwc-api => ./modules/nwc-api

go 1.15

require (
	github.com/go-chi/chi v1.5.4
	github.com/go-chi/cors v1.1.1
	github.com/go-chi/jwtauth v1.2.0
	github.com/go-sql-driver/mysql v1.5.0
	github.com/golang-migrate/migrate v3.5.4+incompatible // indirect
	github.com/jinzhu/gorm v1.9.16
	github.com/rs/zerolog v1.20.0
	github.com/unrolled/render v1.0.3 // indirect
	gitlab.com/newcom-lcs/core/nwc-api v0.0.0-00010101000000-000000000000
	golang.org/x/crypto v0.0.0-20201221181555-eec23a3978ad
	gopkg.in/yaml.v2 v2.2.8
	nhooyr.io/websocket v1.8.7
)
