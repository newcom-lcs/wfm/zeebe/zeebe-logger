package main

import (
	// "flag"
	// "fmt"
	// "net/http"
	"os"
	"time"
	// "strconv"
	// "time"
	"context"

	"github.com/rs/zerolog/log"
	// "gitlab.com/newcom-lcs/clientes/prisma/rm/api.recursos-atm/config"
	"gitlab.com/newcom-lcs/core/nwc-api/app"
	// "gitlab.com/newcom-lcs/core/nwc-api/db"
	// "gitlab.com/newcom-lcs/core/nwc-api/model"

	_ "github.com/go-sql-driver/mysql"
	"github.com/hazelcast/hazelcast-go-client"
	// "github.com/hazelcast/hazelcast-go-client/core"
)

var build string

func init() {
	app.Module.ModuleName = "zeebe-logger"
	app.Module.APIBuild = build
}

func main() {
	//Obtengo el nombre de la base y si no esta seteado defino uno por default
	dbname := os.Getenv("API_DB_NAME")
	if dbname == "" {
		log.Fatal().Msg("API_DB_NAME no especificado")
	}

	// if err := config.Load(); err != nil {
	// 	log.Error().Err(err).Msg("Error al cargar configuracion")
	// }
	// log.Trace().Interface("config", config.Get()).Msg("configuracion")

	/*
		var (
			dbHost     = flag.String("db-host", os.Getenv("API_DB_HOST"), "IP de la BD")
			dbUsername = flag.String("db-username", os.Getenv("API_DB_USERNAME"), "usuario de BD")
			dbSecret   = flag.String("db-secret", os.Getenv("API_DB_SECRET"), "password de la BD")
		)
		flag.Parse()

		log.Debug().Str("dbHost", *dbHost).Msg("Conectando a BD")

		ds := new(db.DataSource)
		ds.Host = dbHost
		ds.Username = dbUsername
		ds.Secret = dbSecret
		ds.DBName = &dbname
		tipo := "MySQL"
		ds.Type = &tipo
		database, _ := db.Open(*ds, "default")
		model.Configure(database)
		defer database.Close()
	*/

	ctx := context.TODO()
	config := hazelcast.Config{}
	config.Cluster.Network.SetAddresses("10.31.37.31:5701")
	client, err := hazelcast.StartNewClientWithConfig(ctx, config)
	if err != nil {
		log.Fatal().Err(err).Msg("Error al conectar a hazelcast")
	}
	defer client.Shutdown(ctx)

	// ringbuffer, _ := client.GetRingbuffer("myRingbuffer")

	// q, err := client.GetQueue(ctx, "zeebe")
	// if err != nil {
	// 	log.Fatal().Err(err).Msg("Error al obtener queue")
	// }

	for {
		time.Sleep(1 * time.Second)
		// item, err := q.Take(ctx)
		// if err != nil {
		// 	log.Fatal().Err(err).Msg("Error al obtener elemento de queue")
		// }
		// log.Debug().Interface("item", item).Msg(".")
	}

}
