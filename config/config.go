package config

import (
	"io/ioutil"

	_log "github.com/rs/zerolog/log"
	"gopkg.in/yaml.v2"
	// "gopkg.in/yaml.v2"
)

var log = _log.With().Str("pkg", "yml").Logger()

//Config contiene todos los parámetros utilizados por la api
type Config struct {
	//SSH contiene la información para conexión ssh desde la api
	SSH struct {
		//Mediadores contiene la lista de mediadores ssh
		Mediadores []struct {
			Name string
			IP   string
			Port *int
		}
		Insecure bool
	}
}

// type SSHMediador struct {
// }

var conf Config

//Load carga la configuración desde archivo config.yml
func Load() error {
	conf = Config{}
	filename := "config.yml"
	content, err := ioutil.ReadFile(filename)
	if err != nil {
		return err
	}

	err = yaml.Unmarshal(content, &conf)
	if err != nil {
		log.Error().Err(err).Msg("")
	}
	return nil
}

//Get retorna la configuración cargada
func Get() Config {
	return conf
}
