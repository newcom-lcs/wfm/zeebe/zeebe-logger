# Go parameters
GOCMD=go
MAIN_FILE=main.go
GOLINT=golint
# GO_SUBPKGS = $(shell go list ./... | grep -v /vendor")
# GOBUILD=$(GOCMD) build
# GOCLEAN=$(GOCMD) clean
# GOTEST=$(GOCMD) test
# GOGET=$(GOCMD) get
# BINARY_NAME=${PWD##*/
# all: test build
build: clean
	$(GOCMD) build -v
install: 
	$(GOCMD) install
test:
	$(GOCMD) test -v -cover ./...
clean:
	$(GOCMD) clean
run: build
	$(GOCMD) run $(MAIN_FILE)
	./$(BINARY_NAME)
lint:
ifneq ("",$(wildcard $(GOPATH)/bin/$(GOLINT)))
	$(GOLINT) -set_exit_status ./... | grep -v /vendor/
else
	$(GOCMD) get -v "golang.org/x/lint/golint"
	@for f in $(GO_SUBPKGS) ; do $(GOLINT) $$f ; done
  # $(GOLINT) -set_exit_status $(go list ./... | grep -v /vendor/)	
endif
.PHONY: doc
doc: 
ifneq ("","$(wildcard $(GOPATH)/bin/godoc)")
	 godoc -http=0.0.0.0:6060 &
	 sleep 3
	 wget -r -np -nH -N -E -p -k -P doc http://127.0.0.1:6060/pkg/gitlab.com/newcom-lcs
else
	 $(GOCMD) get -v "golang.org/x/tools/cmd/godoc"
	 godoc -http=0.0.0.0:6060 &
	 wget -r -np -nH -N -E -p -k -P doc http://127.0.0.1:6060/pkg/gitlab.com/newcom-lcs	
endif
	# wget -r -np -nH -N -E -p -k -P doc http://172.22.0.2:6060/pkg/gitlab.com/newcom-lcs	
deps:
	go get golang.org/x/lint/golint 
	# go get -u golang.org/x/tools

all: clean lint test build
 
